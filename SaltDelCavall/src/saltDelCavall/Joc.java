package saltDelCavall;

import Keyboard.Keyboard;

public class Joc {
	private static int files = 5;
	private static int columnes = 5;
	private static int[][] taulell;
	private static int comptador;
	private static int cordX;
	private static int cordY;
	private static int actualX;
	private static int actualY;
	
	
	public Joc() {
		Joc.taulell = new int[files][columnes];
	}
	
	public static void inicialitzaTaulell(){
		for (int i = 0; i < files; ++i){
			for (int j = 0; j < columnes; ++j){
				taulell[i][j] = 0;
			}
		}
	}
	
	public static boolean comprobarTaulell(int cordX, int cordY){
		if ((cordX < 1 || cordX > 5) || (cordY < 1 || cordY > 5)){
			System.out.println("Error fila i/o columna fora del taullel");
			return false;
		}
		
		else if (taulell[cordX-1][cordY-1] != 0){
			System.out.println("La posicio ja esta ocupada"); 
			return false;
		}
		
		return true;
	}
	
	public static void introduirDades(){
		System.out.print("Entra una cordenada 'x' o retorn per finalitzar: ");
		cordX = Keyboard.readInt();
		System.out.println();
		System.out.print("Entra una cordenada 'y' o retorn per finalitzar: ");
		cordY = Keyboard.readInt();
		System.out.println();
	}
	
	public static void ompleTaulell(){
		for (int i = 1; i <= files; ++i){
			for (int j = 1; j <= columnes; ++j){
				if (i == cordX && j == cordY) taulell[i-1][j-1] = comptador;
			}
		}
	}
	
	public static void pintaTaulell(){
		for (int i = 1; i <= files; ++i) {
			for (int j = 1; j <= columnes; ++j) {
				System.out.print(taulell[i-1][j-1] + "    ");
			}
			System.out.println();
		}
	}
	
	public static boolean comprobarMov(){
		if(actualY-2 == cordY && actualX-1 == cordX) return true;
		if(actualY-2 == cordY && actualX+1 == cordX) return true;
		if(actualY-1 == cordY && actualX-2 == cordX) return true;
		if(actualY-1 == cordY && actualX+2 == cordX) return true;
		if(actualY+1 == cordY && actualX-2 == cordX) return true;
		if(actualY+1 == cordY && actualX+2 == cordX) return true;
		if(actualY+2 == cordY && actualX-1 == cordX) return true;
		if(actualY+2 == cordY && actualX+1 == cordX) return true;
		System.out.println("Moviment impossible!");
		return false;
	}
	
	public static void main(String args[]) {
		Joc joc = new Joc();
		comptador = 1;
		Joc.inicialitzaTaulell();
		Joc.introduirDades();
		while(!Joc.comprobarTaulell(cordX, cordY)){
			Joc.introduirDades();
		}
		Joc.ompleTaulell();
		Joc.pintaTaulell();
		
		while (comptador < 25) {
			actualX = cordX;
			actualY = cordY;
			Joc.introduirDades();
			if (Joc.comprobarTaulell(cordX, cordY) && Joc.comprobarMov()){
				++comptador;
				Joc.ompleTaulell();
				Joc.pintaTaulell();
			}
		}
		if (comptador == 25) System.out.println("Enhorabona! Has guanyat!");
	}
}